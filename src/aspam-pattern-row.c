/* aspam-pattern-row-client.c
 *
 * Copyright 2021 Chris Talbot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "aspam-pattern-row"

#include "aspam-pattern-row.h"
#include "aspam-settings.h"

struct _ASpamPatternRow
{
  AdwActionRow parent_instance;
};

static GType aspam_pattern_row_get_type (void);

G_DEFINE_TYPE (ASpamPatternRow, aspam_pattern_row, ADW_TYPE_ACTION_ROW)

static void
delete_button_clicked_cb (ASpamPatternRow *self)
{
  ASpamSettings *settings;
  const char *pattern;
  GtkWidget *expander_row;
  settings = aspam_settings_get_default ();

  pattern = adw_action_row_get_subtitle (ADW_ACTION_ROW (self));
  aspam_settings_delete_match (settings, pattern);

  /* To dispose: You need to get the parent and have the parent remove it. */
  expander_row = gtk_widget_get_parent (GTK_WIDGET (self));
  gtk_list_box_remove (GTK_LIST_BOX (expander_row), (GTK_WIDGET (self)));
}

static void
aspam_pattern_row_constructed (GObject *object)
{
  //ASpamPatternRow *self = (ASpamPatternRow *)object;

  G_OBJECT_CLASS (aspam_pattern_row_parent_class)->constructed (object);
}

static void
aspam_pattern_row_dispose (GObject *object)
{
  //ASpamPatternRow *self = (ASpamPatternRow *)object;

  G_OBJECT_CLASS (aspam_pattern_row_parent_class)->dispose (object);
}

static void
aspam_pattern_row_class_init (ASpamPatternRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = aspam_pattern_row_constructed;
  object_class->dispose = aspam_pattern_row_dispose;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/kop316/antispam/"
                                               "ui/aspam-pattern-row.ui");

  //gtk_widget_class_bind_template_child (widget_class, ASpamPatternRow, new_whitelist_button);

  gtk_widget_class_bind_template_callback (widget_class, delete_button_clicked_cb);
}

static void
aspam_pattern_row_init (ASpamPatternRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

ASpamPatternRow *
aspam_pattern_row_new (void)
{
  return g_object_new (ASPAM_TYPE_PATTERN_ROW, NULL);
}
